import { Component } from "@angular/core";
import { NavController, Platform } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { FingerprintAIO } from "@ionic-native/fingerprint-aio";
import { WelcomePage } from "../welcome/welcome";
@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  constructor(
    private faio: FingerprintAIO,
    private navCtrl: NavController,
    private storage: Storage,
    private platform: Platform
  ) {
    this.platform.ready().then(() => {
      this.faio
        .show({
          clientId: "Fingerprint-Demo",
          clientSecret: "password",
          disableBackup: false,
          localizedFallbackTitle: "Use Pin",
          localizedReason: "Please authenticate"
        })
        .then((result: any) => this.goTo())
        .catch((error: any) => this.goTo());
    });
  }

  goTo() {
    this.navCtrl.setRoot(WelcomePage).then(() => {});
  }
}
